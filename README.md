Sammlungsportal OAI Scripts
===========================

# Harvester script

This script can be used to visualize the source systems of the Forum Wissen collection.


Located in `scripts`, needs the dependencies from `requirements.txt`, install using `python -m pip install -r scripts/requirements.txt`. Run using Python: `python scripts/harvester.py`.

It's creates the following results:

```
%% Processed 467 records
%% Mermaid config
pie title Objekte im Sammlungpotal nach Quelle
    "kuniweb" : 252
    "naniweb" : 214
```

The results can be visualised using [Mermaid](https://mermaid.live/edit).

# Docker images

The [Docker documentation](/docs/docker.md) has been moved to the [`docs`](/docs) directory.

## Quick start

In a shell run:

```
git clone https://gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai.git
cd sammlungsportal-oai
docker-compose up
```

You now can connect to exist in your browser at [http://localhost:8081/exist/](http://localhost:8081/exist/).
On the eXist start page you can choose [eXide](http://localhost:8081/exist/apps/eXide/index.html), the you can run and test queries.

**Warning: Using the quick start approach can lead to data loss, make sure to save queries externally as well!**

# Starting all images

```
docker-compose up
```

# TODO
* Fix Jupyter Lab base URL
* fix eXist url mapping
* Integrate more / improve examples
    * Links in KML
    * Statistics on Geodate by Source / Sammlung

# Examples
The examples have been moved to [it's own page](docs/examples.md)
