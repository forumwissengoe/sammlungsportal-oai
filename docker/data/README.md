Data Images
===========

# GDZ data

## Python command

```
python3 ./harvester.py -e https://gdz.sub.uni-goettingen.de/oai2/ -m mets -d gdz.sub -j 6
```

## Docker command

```
docker buildx build --build-arg ENDPOINT=https://gdz.sub.uni-goettingen.de/oai2/ --build-arg METADATA_PREFIX=mets --build-arg DIRECTORY=gdz.sub --build-arg JOBS=6 -f docker/data/Dockerfile .
```

# Sammlungsportal data

## Python command

```
python3 ./harvester.py -e https://sammlungen.uni-goettingen.de/oai/ -u "${USER}" -p "${PASS}" -m lido -d sammlungsportal
```

## Docker command

```
docker buildx build --build-arg ENDPOINT=https://sammlungen.uni-goettingen.de/oai/ --build-arg METADATA_PREFIX=lido --build-arg DIRECTORY=sammlungsportal --build-arg USER="${USER}" --build-arg PASSWORD="${PASS}" -f docker/data/Dockerfile .
```

