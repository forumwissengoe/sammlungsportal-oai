#/usr/bin/env python

from lxml import etree
import sys, os, logging, argparse, pathlib
from multiprocessing import Manager, Pool, Lock, Value
from queue import Empty
from time import sleep
from oaipmh_scythe import Scythe
from oaipmh_scythe.models import Record
from oaipmh_scythe.exceptions import BadResumptionToken
from httpx import ConnectTimeout, ConnectError, ReadTimeout, HTTPStatusError
from httpcore import ReadTimeout as HttpcoreReadTimeout

from termcolor import cprint

LOG_FILE= '../harvester.log'
JOBS = 8
logging.basicConfig(filename=LOG_FILE, format='%(levelname)s %(asctime)s %(process)s %(message)s', level=logging.INFO)
output_dir = '.'
default_metadata_prefix = 'oai_dc'

class XML(Record):
    namespaces = {'lido': 'http://www.lido-schema.org',
              'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
              'oai': 'http://www.openarchives.org/OAI/2.0/'}

    def __init__(self, record_element: etree._Element, strip_ns: bool = False) -> None:
        super().__init__(record_element, strip_ns=strip_ns)
        self.metadata_xml = etree.ElementTree(self.xml.find(".//" + self._oai_namespace + "metadata").getchildren()[0])
        self.identifier = self.header.identifier

# See https://eli.thegreenplace.net/2012/01/04/shared-counter-with-pythons-multiprocessing
class Counter(object):
    def __init__(self, initval=0):
        self.val = Value('i', initval)
        self.lock = Lock()

    def increment(self):
        with self.lock:
            self.val.value += 1

    def value(self):
        with self.lock:
            return self.val.value

def worker(q, download_counter):
    while True:
        try:
            item = q.get(timeout=10)
        except Empty:
            break
        if item is None:
            break
        endpoint, identifier, metadata_prefix, output_dir, auth, wait = item
        oai = init_harvester(endpoint, auth)
        try:
            record = oai.get_record(identifier=identifier, metadata_prefix=metadata_prefix)
            path = pathlib.Path(output_dir)
            filename = path.joinpath(f"{record.identifier}.{metadata_prefix}.xml")
            record.metadata_xml.write(filename, encoding="utf-8")
            with Lock():
                logging.debug(f"Sucessfully got {record.identifier}, saved to {filename}")
            cprint('.', 'green', end='', flush=True)
            if wait > 0:
                sleep(wait / 1000)
            download_counter.increment()
        except (ConnectTimeout, ConnectError, ReadTimeout, HTTPStatusError) as e:
            q.put(item, block=True)
            with Lock():
                logging.error(f"{type(e).__name__} getting {identifier}, requeueing")
            cprint('X', 'red', end='', flush=True)
        except StopIteration as e:
            q.put(item, block=True)
            with Lock():
                logging.error(f"{type(e).__name__} getting {identifier}, requeueing")
            cprint('x', 'yellow', end='', flush=True)
        finally:
            oai.close()
        if download_counter.value() % 1000 == 0:
            cprint(f"\n{download_counter.value()}", 'green', flush=True)

def init_harvester(endpoint, auth=None, retries=5, timeout=120) -> Scythe:
    oai = Scythe(endpoint, timeout=timeout, max_retries=retries, retry_status_codes=(500, 503, 504), auth=auth)
    oai.class_mapping['GetRecord'] = XML
    if auth is not None:
        USER, PASSWORD = auth
        client = oai.client
        client.auth=(USER, PASSWORD)
        oai._client = client
    return oai

def main() -> int:
    global JOBS
    parser = argparse.ArgumentParser(prog='harvester.py')
    parser.add_argument('--endpoint', '-e', required=True, help='OAI endpoint')
    parser.add_argument('--user', '-u', help='OAI user')
    parser.add_argument('--password', '-p', help='OAI password')
    parser.add_argument('--directory', '-d', help='Output directory')
    parser.add_argument('--set', '-s', default=None, help='Set to retrieve')
    parser.add_argument('--jobs', '-j', default=JOBS, type=int, help=f"Number of Jobs, defaults to {JOBS}, note that there is always another thread to collect identifiers")
    parser.add_argument('--metadata_prefix', '-m', default="oai_dc", help='Metadata prefix (format)')
    parser.add_argument('--list-dc', '-l', default=True, action='store_true', help='Use OAI DC in listing')
    parser.add_argument('--verbose', '-v', action='count', default=0, help='Be verbose')
    parser.add_argument('--wait', '-w', type=int, help='Wait time after each request in ms')
    parser.add_argument('--queue-first', '-q', default=False, action='store_true', help='Build request queue completely before download')
    args = parser.parse_args()
    set = None
    init_log_suffix = ''
    queue_first = False
    wait = 0

    if args.verbose:
        verbose_handler = logging.StreamHandler(sys.stdout)
        verbose_handler.setFormatter(logging.Formatter('%(levelname)s %(asctime)s %(process)s %(message)s'))
        
        if args.verbose > 1:
            logging.getLogger().setLevel(logging.DEBUG)
        # the logger `oaipmh_scythe.utils` will be shown
        if args.verbose < 3:
            logging.getLogger('httpx').setLevel(logging.WARN)
            logging.getLogger('httpcore').setLevel(logging.WARN)
        if args.verbose > 2:
            verbose_handler.setFormatter(logging.Formatter('%(name)s: %(levelname)s %(asctime)s %(process)s %(message)s'))
        logging.getLogger().addHandler(verbose_handler)  
    if args.directory:
        pathlib.Path(args.directory).mkdir(parents=True, exist_ok=True)
        output_dir = args.directory
    else:
        output_dir = '.'
    if args.user and args.user == '':
        logging.warning(f"Passed empty user, no authentification will be used")
        USER = ''
    elif args.user and args.user != '':
        USER = args.user
    else:
        USER = ''
    if args.password:
        if args.password == '':
            logging.warning(f"Passed empty password!")
        PASSWORD = args.password
    if args.endpoint:
        endpoint = args.endpoint
    if args.set and args.set != '':
        set = args.set
    if args.metadata_prefix and args.metadata_prefix != '':
        metadata_prefix = args.metadata_prefix
    else:
        metadata_prefix = default_metadata_prefix
    if args.list_dc and args.list_dc is False:
        list_metadata_prefix = metadata_prefix
        init_log_suffix = f", formatv for listing is set to {list_metadata_prefix}"
    else:
        list_metadata_prefix = default_metadata_prefix
    if args.queue_first:
        queue_first = args.queue_first
        init_log_suffix = f"{init_log_suffix}, starting workers after building queue"
    if args.wait and args.wait != 0:
        wait = args.wait
        init_log_suffix = f"{init_log_suffix}, waiting after each request for {wait}ms"

    if args.jobs and args.jobs != 0:
        JOBS = args.jobs
        logging.info(f"Setting number of jobs to {JOBS}")

    if USER != '' or (os.getenv('OAI_USER') is not None and os.getenv('OAI_USER') != ''):
        if os.getenv('OAI_USER') is not None and os.getenv('OAI_USER') != '':
            USER = os.getenv('OAI_USER')
            if os.environ.get('OAI_PASSWORD') is not None and os.environ.get('OAI_PASSWORD') != '':
                PASSWORD = os.environ.get('OAI_PASSWORD')
            else:
                PASSWORD = ''     
            logging.info(f"Using auth data from env for {USER}")
        if PASSWORD is None:
            PASSWORD = ''
        try: PASSWORD
        except:
            logging.warn(f"No password provided for {USER}, using empty one!")
            PASSWORD = ''
        init_log_suffix = f"{init_log_suffix}, using auth data for {USER}"
        auth = (USER, PASSWORD)
    else:
        init_log_suffix = f"{init_log_suffix}, disabled auth!"
        auth = None
    if args.verbose < 2:
        args.password = 'xxxxxxxxxxxx'
    logging.debug('Called with args: %s', args)
    logging.info(f"Intializing Harvester for {endpoint}, format {metadata_prefix}, output directory {output_dir}{init_log_suffix}")
    oai = init_harvester(endpoint, auth, retries=10)
    download_counter = Counter(0)
    m = Manager()
    q = m.Queue()
    if not queue_first:
        pool = Pool(JOBS, worker, (q, download_counter))
    else:
        logging.info(f"Building queue before downloading")
    if set is not None and set != '':
        logging.info(f"Collecting set '{set}'")
        records = oai.list_identifiers(ignore_deleted=True, metadata_prefix=list_metadata_prefix, set_=set)
    else:
        records = oai.list_identifiers(ignore_deleted=True, metadata_prefix=list_metadata_prefix)
    i = 0
    try:
        for record in records:
            q.put((endpoint, record.identifier, metadata_prefix, output_dir, auth, wait))
            i += 1
    except (ReadTimeout, HttpcoreReadTimeout, ConnectError, HTTPStatusError) as e:
        additional_text = ''
        if isinstance(e, HTTPStatusError):
            additional_text = str(e.response.status_code)
        logging.error(f"{type(e).__name__} listing records ({additional_text}), exiting!")
        cprint(f"{type(e).__name__} listing records ({additional_text}), exiting", 'red')
        with open(LOG_FILE) as f:
            for line in (f.readlines() [-10:]):
                cprint(line,  'red', end ='')
        cprint(f"\nDownloaded: {download_counter.value()}, collected {i}", 'yellow')
        return 666
    except BadResumptionToken:
        logging.error(f"Bad resumption token during listing, exiting!")
        cprint(f"Bad resumption token during listing, exiting", 'red')
        cprint(f"\nDownloaded: {download_counter.value()}, collected {i}", 'yellow')
        return 1155
    except KeyboardInterrupt as e:
        pool.terminate()
        pool.join()
        cprint(f"\nDownloaded: {download_counter.value()}, collected {i}", 'yellow')
        logging.warning(f"Interuped by user ({type(e).__name__})")
        logging.info(f"Downloaded: {download_counter.value()}, collected {i}")
        return 1
    logging.info(f"Queue filled: {i} entries")
    cprint(f"\nQueue filled: {i} entries", 'yellow')
    if queue_first:
        pool = Pool(JOBS, worker, (q, download_counter))
    while not q.empty():
        sleep(1)
    pool.close()
    pool.join()
    logging.info(f"Downloaded: {download_counter.value()} entries")

if __name__ == '__main__':
    sys.exit(main())
