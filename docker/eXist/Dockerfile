# syntax=docker/dockerfile:experimental

FROM maven:3.6-openjdk-17 AS builder

ENV EXIST_GIT_URL=https://github.com/eXist-db/exist.git \
    EXIST_GIT_TAG=eXist-6.3.0 \
    BUILD_DIR=/tmp/build \
    EXIST_HOME=/opt/exist \
    SOLR_VERSION=9.2.1 \
    REQ_BUILD="bzip2 xmlstarlet dnf"

ADD docker/eXist/dependencies/pom.xml $BUILD_DIR/

RUN microdnf install $REQ_BUILD && \
    mkdir -p $BUILD_DIR $EXIST_HOME && \
# Setup package manager - WTF?!
    mkdir -p /etc/dnf/vars && \
    echo "" > /etc/dnf/vars/ociregion && \
    dnf -y module enable nodejs:16 && \
    dnf --setopt=install_weak_deps=False -y install nodejs nodejs-nodemon npm && \
# More dependencies 
    cd $BUILD_DIR && \
    # TODO: Check why this won't work
    #npm install --global @existdb/xst @existdb/node-exist && \
# Install Exist
    git clone --recurse-submodules $EXIST_GIT_URL --branch $EXIST_GIT_TAG --single-branch --depth=1 && \
    cd exist && \
    # Add this for develop branch -pl '!:indexes-integration-tests'
    CI=true mvn -B --no-transfer-progress -T 2C -Ddependency-check.skip=true -DskipTests=true clean install package && \
    mv $BUILD_DIR/exist/exist-distribution/target/exist-distribution-*-unix.tar.bz2 $EXIST_HOME && \
    cd $EXIST_HOME && \
    tar xjf exist-distribution-*-unix.tar.bz2 --strip-components=1 && \
    rm -f exist-distribution-*-unix.tar.bz2 && \
# Get Solr drivers, see https://nightlies.apache.org/solr/draft-guides/solr-reference-guide-antora/solr/10_0/query-guide/sql-query.html#generic-clients
    mkdir - $BUILD_DIR/solr && \
    mv $BUILD_DIR/pom.xml $BUILD_DIR/solr && \
    cd $BUILD_DIR/solr && \
    mvn -B --no-transfer-progress package && \
    mv target/appassembler/booter-unix/etc/app/lib . && \
    xmlstarlet sel -t -c "/daemon/classpath" target//appassembler/booter-unix/etc/app.xml |xmlstarlet ed -d "/classpath/dependencies/dependency[1]" > ./classpath.xml  && \ 
    rm -r pom.xml target
#RUN mv lib/* $EXIST_HOME/lib/ && \

FROM alpine:3.19

LABEL maintainer="mahnke@kustodie.uni-goettingen.de"
LABEL org.opencontainers.image.source=https://gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai

ENV REQ_BUILD="wget" \
    RUN_DEPS="openjdk17-jdk busybox bash gosu" \
    EXIST_HOME=/opt/exist \
    EXIST_DATA_DIR="data" \
    EXIST_USER=exist

COPY --from=builder $EXIST_HOME $EXIST_HOME/

ADD docker/eXist/scripts/exist_passwd.sh /usr/local/bin/

RUN echo 'https://dl-cdn.alpinelinux.org/alpine/edge/testing/' >> /etc/apk/repositories && \
    apk --update upgrade && \
    apk add --no-cache $RUN_DEPS $REQ_BUILD && \
    addgroup -g 1000 -S $EXIST_USER && \
    adduser -u 1000 --no-create-home -S $EXIST_USER -G $EXIST_USER && \
    mkdir /entrypoint.d $EXIST_HOME/$EXIST_DATA_DIR && \
    wget -O /usr/local/bin/wait-for-it.sh https://raw.githubusercontent.com/vishnubob/wait-for-it/81b1373f17855a4dc21156cfe1694c31d7d1792e/wait-for-it.sh && \
    chmod +x /usr/local/bin/wait-for-it.sh && \
    chown -R $EXIST_USER:$EXIST_USER $EXIST_HOME && \
    apk del ${REQ_BUILD} && \
    rm -rf /var/cache/apk/* 

USER ${EXIST_USER}
EXPOSE 8080 8443
VOLUME ${EXIST_HOME}/${EXIST_DATA_DIR}