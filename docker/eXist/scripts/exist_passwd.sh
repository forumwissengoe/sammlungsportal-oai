#!/bin/sh

EXIST_ADMIN_PASSWORD=$1

cd /
$EXIST_HOME/bin/startup.sh &
/usr/local/bin/wait-for-it.sh -t 60 localhost:8080
printf "passwd admin\n$EXIST_ADMIN_PASSWORD\n$EXIST_ADMIN_PASSWORD\nquit\n" | /opt/exist/bin/client.sh -s
$EXIST_HOME/bin/shutdown.sh