#!/bin/sh

export PATH=$PATH:$SPARK_HOME/bin:/usr/local/bin

cd
if [ -n "$JUPYTER_PASSWORD" ] ; then
    printf "$JUPYTER_PASSWORD\n$JUPYTER_PASSWORD" |jupyter lab password 
fi
$SPARK_HOME/bin/pyspark

if [ -n "$@" ] ; then
    $@
fi