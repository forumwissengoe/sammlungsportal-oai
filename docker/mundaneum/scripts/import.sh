#!/bin/sh

cd /
gosu $EXIST_USER $EXIST_HOME/bin/startup.sh &
/usr/local/bin/wait-for-it.sh -t 120 localhost:8080 -- echo "Exist has started"
printf "mkcol data\nquit\n" | /opt/exist/bin/client.sh -s -ouri=xmldb:exist://localhost:8080/exist/xmlrpc
for DIR in `find /data -type d -mindepth 1`
do
    IMPORT_DIR=`basename $DIR`
    echo "Importing from $IMPORT_DIR ($DIR)"
    printf "mkcol data/$IMPORT_DIR\nquit\n" | /opt/exist/bin/client.sh -s -ouri=xmldb:exist://localhost:8080/exist/xmlrpc
    printf "cd data/$IMPORT_DIR\nput /data/$IMPORT_DIR/**\nquit" | /opt/exist/bin/client.sh -s -ouri=xmldb:exist://localhost:8080/exist/xmlrpc
done
if [ -r /setup/xquery ] ; then 
    echo "Importing XQUERY"
    printf "mkcol mudaneum\nquit\n" | /opt/exist/bin/client.sh -s -ouri=xmldb:exist://localhost:8080/exist/xmlrpc
    printf "cd mudaneum\nput /setup/xquery/**\nquit" | /opt/exist/bin/client.sh -s -ouri=xmldb:exist://localhost:8080/exist/xmlrpc
fi
$EXIST_HOME/bin/shutdown.sh