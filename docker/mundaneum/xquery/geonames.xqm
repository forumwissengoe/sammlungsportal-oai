import module namespace functx="http://www.functx.com";

declare namespace map = 'http://www.w3.org/2005/xpath-functions/map';
declare namespace array = 'http://www.w3.org/2005/xpath-functions/array';


module namespace geonames = "http://mundaneum.forum-wissen.de/sammlungsportal/geonames";

declare variable $geonames:default-query-prefix := 'http://geonames:8983/solr/geonames/query?q='

declare function geonames:fix-geonames-uri($uri as xs:string) as xs:string
{
    if (ends-with($uri, '/')) then 
        $uri
    else
        concat($uri, '/')
};

(: See http://www.xqueryfunctions.com/xq/functx_replace-multi.html :)
declare function geonames:replace-multi
  ( $arg as xs:string? ,
    $changeFrom as xs:string* ,
    $changeTo as xs:string*,
    $flags as xs:string?)  as xs:string? {

    if (count($changeFrom) > 0)
       then geonames:replace-multi(
              replace($arg, $changeFrom[1],
                         functx:if-absent($changeTo[1],''), $flags),
              $changeFrom[position() > 1],
              $changeTo[position() > 1],
              $flags)
       else $arg
};

declare function geonames:escape-solr-query($str as xs:string) as xs:string
{
    let $match := ('\', '+', '-', '&amp;', '|', '!', '(', ')', '{', '}', '[', ']', '^', '~', '*', '?', ':', '"', ';', ' ')
    let $replace := ('\\', '\+', '\-', '\&amp;', '\|', '\!', '\(', '\)', '\{', '\}', '\[', '\]', '\^', '\~', '\*', '\?', '\:', '\"', '\;', '\ ')
    return geonames:replace-multi($str, $match, $replace, 'q')
};

declare function local:get-geoname($uri as xs:string) as map()
{
    let $prefix := 'http://geonames:8983/solr/geonames/query?q='
    let $geonames-uri := local:fix-geonames-uri($uri)
    let $escaped-geonames-uri := local:escape-solr-query($geonames-uri)
    let $url := concat($prefix, 'uri:', $escaped-geonames-uri)
    let $response-docs := json-doc($url)('response')('docs')
    
    return 
    if (array:size($response-docs) = 1) then
        array:get($response-docs, 1)
    else map{}
}; 

(:
<kml xmlns="http://www.opengis.net/kml/2.2">{
for $doc in collection("/db/data/sammlungsportal") where $doc//lido:eventPlace/lido:place/lido:placeID
    let $id := $doc//lido:lidoRecID/text()
    let $name := $doc//lido:objectIdentificationWrap/lido:titleWrap/lido:titleSet[@lido:sortorder='1']/lido:appellationValue[1]
    let $description := $doc//lido:objectDescriptionWrap//lido:descriptiveNoteValue[1]

    return 
    for $place in $doc//lido:eventPlace/lido:place/lido:placeID
        let $result-doc := local:get-geoname($place/text())
        let $coordinate := $result-doc('coordinate')
        return
        <Placemark>
            <name>{$name}</name>
            <description>{$description}</description>
            <Point>
                <coordinates>{$coordinate}</coordinates>
            </Point>
        </Placemark>

}</kml>
:)