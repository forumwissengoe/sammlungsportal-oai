xquery version "3.1";

import module namespace packages = 'http://exist-db.org/apps/existdb-packages' at '/db/apps/packageservice/modules/packages.xqm';

declare function local:check-installed($uri as xs:string) as xs:boolean
{
    if (packages:get-local-packages()[@url = $uri]) then 
        true()
    else
        false()
};

(:
local:check-installed('http://www.functx.com')

for $p in packages:get-local-packages()
return (data($p/@url), data($p/@path))
:)