Installation / Building
=======================

# Intoduction

This page describes the singe provided images and how to build them.
The building step is only required to enhance or debug the images. These images are also build by the GitLab CI, so there is no need to buzild them yourself. You might need to log in into the GitLab Docker Registry to download the prebuild images.


# Installation

Since the following images are build using the GitLab CI, its possible to use pre build images, use `docker-compose` to download them. This has also the benefit that only required once are pulled:

```
docker-compose pull
```

# Docker `data` image (s)

This images either contains all Lido data of the Sammlungsportal or all METS Records from the GDZ portal as files

## Tag (Name)

For Sammlungsportal data:
`docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/sammlungsportal-data`

For GDZ Data

`docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/gdz.sub-data`

## Status

Currently the build with the GDZ data fails, due to server issues, this is currently under investigation by the SUB.

## Sammlungsportal data

### Configuration

You can preconfigure the login data in `docker/data/config/config.yaml`, but the prefered method is now to pass configuration as build args, see section "Building" below

### Building

```
docker build -t docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/sammlungsportal-data:latest -f docker/data/Dockerfile . 
```

You can also pass the configuration as 

```
docker build --build-arg USER=user --build-arg PASSWORD=password --build-arg ENDPOINT=https://sammlungen.uni-goettingen.de/oai/ --build-arg METADATA_PREFIX=lido --build-arg DIRECTORY=sammlungsportal -t docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/sammlungsportal-data:latest -f docker/data/Dockerfile .
```

To limit the contents of the Sammlungsportal, pass the SET option: `--build-arg SET=fowi`

## GDZ data

**Note: This is currently not working**

### Building

```
docker build --build-arg ENDPOINT=https://gdz.sub.uni-goettingen.de/oai2/ --build-arg METADATA_PREFIX=mets --build-arg DIRECTORY=gdz.sub --build-arg JOBS=6 --build-arg WAIT=50 -f docker/data/Dockerfile -t docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/gdz.sub-data:latest .
```

# Docker `mundaneum` image

This image contains all gathered data in an eXist Database

## Tag (Name)

`docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/mundaneum:latest`

This image provides a XML database containing all Lido records from the Sammlungsportal and all METS Records from the GDZ portal.
Make sure to have build the `data` Image before, since the GitLAb Docker registry is not publically available yet.

## Building

```
docker build -f docker/mundaneum/Dockerfile . -t docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/mundaneum:latest
```

# Docker `eXist` image

This image is not used directlly but as a base image for the `mundaneum` image.

## Tag (Name)

`docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/exist:latest`

## Building

```
docker build . -f docker/eXist/Dockerfile -t docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/exist:latest
```

## Development `exist` image

It's also possible to build the development version of exist, this allows to run `exist` with a recent Java version, since a [related bug](https://github.com/eXist-db/exist/issues/5249) has been fixed.

### Tag (Name)

`docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/exist:develop`

### Building

```
docker build . -f docker/eXist/Dockerfile.develop -t docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/exist:develop
```

# Docker `mundaneum-spark` image

This is currently unfinished and will provide a [Apache Spark](https://spark.apache.org/) instance on top of the provided data.

## Tag (Name)

`docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/mundaneum-spark:latest`

## Building

```
docker build . -f docker/eXist/Dockerfile -t docker.gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai/mundaneum-spark:latest
```