Docker Usage
============

This page just describes the current status, unfinished work isn't documented yet. 

# Mundaneum (eXist Base image)

The main functionality is currently provided by the [eXist XML database](https://exist-db.org/). This provides a possibility to execute [XQuery](https://en.wikipedia.org/wiki/XQuery) on all provided metadata files, like a complete set of the Sammlungsportal. 

So start you just need to run the following command. This will take some time on the first start, since all required images (including the data) need to be pulled from the GitLab container registry.

```
docker-compose up
```

# Services

There are currently two services available:
* A complete dump of [Geonames](https://www.geonames.org/) packaged in a [Solr search server](https://solr.apache.org/).
* A complete dump of the [Gemeinsame Normdatei (GND)](https://www.dnb.de/EN/Professionell/Standardisierung/GND/gnd_node.html) in a [Jena Triple Store](https://jena.apache.org/) with a [Fuseki SPARQL Endpoint](https://jena.apache.org/documentation/fuseki2/)

These can be started using docker compose as well. This will take some time on the first start, since all required images (including the data) need to be pulled from the GitHub container registry. Currently these images need additional 9 GB of storage.

Together with the XML database: 

```
docker-compose -f docker-compose.yaml -f docker-compose.services.yaml up
```

Only the services:

```
docker-compose -f docker-compose.services.yaml up
```

## Usage

### Geonames Container

#### Get all available information for a city:

http://localhost:8080/geonames/query?debug=query&q=n:G%C3%B6ttingen

#### Get a record by it's URI

http://localhost:8080/geonames/query?debug=query&q=uri: