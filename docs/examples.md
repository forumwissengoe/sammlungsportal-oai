XQuery Examples
===============

# A list of unique places for events
```
xquery version "3.1";

declare namespace lido = 'http://www.lido-schema.org';

let $uris := for $x in collection("/db/data/sammlungsportal")
    let $z := $x//lido:eventPlace/lido:place/lido:placeID
    return $z/text()

return distinct-values($uris)
```

# Extracting coordinates from Events into KML

The following extracts geo points from each event.

```
xquery version "3.1";

declare namespace gml = "http://www.opengis.net/gml";
declare namespace lido = 'http://www.lido-schema.org';


<kml xmlns="http://www.opengis.net/kml/2.2">{
for $doc in collection("/db/data/sammlungsportal") where $doc//lido:eventPlace/lido:place/lido:placeID
    let $id := $doc//lido:lidoRecID/text()
    let $doc-uri := $doc//lido:objectPublishedID[1]/text()
    let $name := $doc//lido:objectIdentificationWrap/lido:titleWrap/lido:titleSet[@lido:sortorder='1']/lido:appellationValue[1]/text()
    let $description := $doc//lido:objectDescriptionWrap//lido:descriptiveNoteValue[1]/text()

    return
    <Document> {
    for $place in $doc//lido:eventPlace/lido:place/lido:gml
        let $coordinate := string-join(reverse(tokenize($place/gml:Point/gml:pos/text(), '\s+')), ',')
        return
        <Placemark>
            <name>{$name}</name>
            <description>{$description}</description>
            <Point>
                <coordinates>{$coordinate}</coordinates>
            </Point>
        </Placemark>
    }</Document>
}</kml>
```

## Result

[Resulting KML](results/Events.kml) file in Google Earth

![KML](img/event-kml.png)

# Get all sources of records

Visualise the souces of records

```
xquery version "3.1";

declare namespace lido = 'http://www.lido-schema.org';

(: 
Special charakters:
* \n &#xa;
* \t &#xd;
* " &quot;
:)

let $body := for $source in distinct-values(collection("/db/data/sammlungsportal")//lido:lido/lido:administrativeMetadata/lido:recordWrap/lido:recordSource/lido:legalBodyName/lido:appellationValue/text())
    let $count := count(collection("/db/data/sammlungsportal")//lido:lido/lido:administrativeMetadata/lido:recordWrap/lido:recordSource/lido:legalBodyName/lido:appellationValue[text() = $source])
    order by $count descending
    
    return concat("&#xd;&quot;", $source, "&quot; : ", $count)

return concat("%% Mermaid config&#xa;", "pie title Objekte im Sammlungpotal nach Quelle&#xa;", string-join($body))
```

## Result

![Source distribution](img/source-distribution.png)

# Analyse the links in the records


```
xquery version "3.1";

declare namespace lido = 'http://www.lido-schema.org';

(: 
Special charakters:
* \n &#xa;
* \t &#xd;
* " &quot;
:)

for $x in collection("/db/data/sammlungsportal")//lido:lido
  return string-join( ($x/lido:lidoRecID/text(), $x//lido:objectWebResource/text()), " | ")
```
  