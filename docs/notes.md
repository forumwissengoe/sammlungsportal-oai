Development notes
=================

This page just containes some notes.

# Links to check

*[Quarto](https://quarto.org/)

# XQuery


```
xquery version "3.1";

declare namespace lido = 'http://www.lido-schema.org';
declare namespace request = 'http://exist-db.org/xquery/request';

import module namespace http="http://expath.org/ns/http-client";
import module namespace functx="http://www.functx.com";

declare function local:fix-geonames-uri($uri as xs:string) as xs:string
{
    if (ends-with($uri, '/')) then 
        $uri
    else
        concat($uri, '/')
};

(: http://www.xqueryfunctions.com/xq/functx_replace-multi.html :)
declare function local:escape-solr-query($str as xs:string) as xs:string
{
    $match = ('\\', '+', '-', '&amp;', '|', '!', '(', ')', '{', '}', '[', ']', '^', '~', '*', '?', ':', '"', ';', ' ')
    $replace = ('\\\\', '\\+', '\\-', '\\&amp;', '\\|', '\\!', '\\(', '\\)', '\\{', '\\}', '\\[', '\\]', '\\^', '\\~', '\\*', '\\?', '\\:', '\\"', '\\;', '\\ ')
    functx:replace-multi($str, $match, $replace)

};

let $prefix := 'http://geonames:8983/solr/geonames/query?q='

(: 
Special charakters:
* \n &#xa;
* \t &#xd;
* " &quot;
:)
for $doc in collection("/db/data/sammlungsportal") where $doc//lido:eventPlace/lido:place/lido:placeID
    let $id := $doc//lido:lidoRecID/text()
    for $place in $doc//lido:eventPlace/lido:place/lido:placeID
        let $geonames-uri := local:fix-geonames-uri($place/text())
        let $escaped-geonames-uri := local:escape-solr-query($geonames-uri)
        let $url := concat($prefix, 'uri:', $escaped-geonames-uri)

        let $json := json-doc($url)
        return $json

    (: parse-json( :)

```

```
xquery version "3.1";

declare namespace util = 'http://exist-db.org/xquery/util';
declare namespace lido = 'http://www.lido-schema.org';


for $x in collection("/db/data/*")
(:  let $z := $x//lido:eventPlace:)
return $x

```