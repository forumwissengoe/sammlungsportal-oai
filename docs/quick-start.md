Quick start guide
=================

# Prerequisites
To run the dump of the Sammlungsportal locally you need two software packages:
* [Git](https://git-scm.com/download/) - a soucre code management system
* [Docker](https://www.docker.com/products/docker-desktop/)  - a runtime enviroment for Linux containers

# Restart
After installing it's recommended to restartt your machine to make sure all varianles have been set

# Opening a terminal and create a working directory

Run the command `cmd` on Windows and create a empty directory to work in:

```
mkdir "%USERPROFILE%"\sammlungsportal
```

Change into this directory:

```
cd "%USERPROFILE%"\sammlungsportal
```

# Checkout the source code

This checksout the source code from the remote repository:

```
git clone https://gitlab.gwdg.de/forumwissengoe/sammlungsportal-oai.git
```

# Starting the continer

The last step is to start the container:

```
docker-compose -f docker-compose.yaml
```