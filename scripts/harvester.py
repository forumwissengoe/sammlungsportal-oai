#/usr/bin/env python

from lxml import etree
from oaipmh_scythe import Scythe
from oaipmh_scythe.models import Record

class LIDO_XML(Record):
    namespaces = {'lido': 'http://www.lido-schema.org',
              'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
              'oai': 'http://www.openarchives.org/OAI/2.0/'}

    def __init__(self, record_element: etree._Element, strip_ns: bool = False) -> None:
        super().__init__(record_element, strip_ns=strip_ns)
        self.metadata_element = etree.ElementTree(self.xml.find(".//" + self._oai_namespace + "metadata").getchildren()[0])
        self.identifier = self.header.identifier
        
    def xpath(self, path, namespaces=None):
        if namespaces is None:
            namespaces = self.namespaces
        return self.metadata_element.xpath(path, namespaces=namespaces)


oai = Scythe('https://sammlungen.uni-goettingen.de/oai/')
oai.class_mapping['ListRecords'] = LIDO_XML

client = oai.client
client.auth=('oai', 'oaipasswort')
oai._client = client

records = oai.list_records(metadata_prefix='lido', set_='fowi')

sources = {}
i = 1
for record in records:
    source = record.xpath('//lido:lido/lido:administrativeMetadata/lido:recordWrap/lido:recordSource/lido:legalBodyName/lido:appellationValue')[0]
    sourceName = getattr(source, "text", None)
    if sourceName in sources:
        sources[sourceName].append(record.identifier)
    else:
        sources[sourceName] = []
        sources[sourceName].append(record.identifier)   
    i += 1
    if not (i % 10):
        print('.', end="", flush=True)

print(f'\n%% Processed {i} records')

print("%% Mermaid config")
print("pie title Objekte im Sammlungpotal nach Quelle")
for source in sources.keys():
    print(f'    "{source}" : {len(sources[source])}')
